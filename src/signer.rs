use bip32::{ChildNumber, Prefix, XPrv};
use bip39::{Language, Mnemonic, Seed};
use libsecp256k1::{recover, sign, Message, PublicKeyFormat, RecoveryId, SecretKey, Signature};
use ring::digest;
use std::convert::TryInto;
use wasm_bindgen::UnwrapThrowExt;

const PATH_BASE: &'static str = "m/44'/3757'/0'";
pub fn key_from_entropy(entropy: &[u8], idx: u32) -> [u8; 32] {
    let mnemonic = Mnemonic::from_entropy(entropy, Language::English).unwrap_throw();
    let seed = Seed::new(&mnemonic, "");

    assert_eq!(seed.as_bytes().len(), 64);
    println!("{}", hex::encode(seed.as_bytes()));

    let child_xprv =
        XPrv::derive_from_path(&seed, &PATH_BASE.parse().unwrap_throw()).unwrap_throw();

    // Serialize `child_xprv` as a string with the `xprv` prefix.
    let child_xprv_str = child_xprv.to_string(Prefix::XPRV);
    assert!(child_xprv_str.starts_with("xprv"));
    // println!("xprv: {}", child_xprv_str.as_str());

    // // Get the `XPub` associated with `child_xprv`.
    // let child_xpub = child_xprv.public_key();

    // // Serialize `child_xpub` as a string with the `xpub` prefix.
    // let child_xpub_str = child_xpub.to_string(Prefix::XPUB);
    // assert!(child_xprv_str.starts_with("xprv"));

    // println!("xpub: {}", child_xpub_str.as_str());

    // now derive the wallet
    let child = child_xprv
        .derive_child(ChildNumber(0))
        .unwrap_throw()
        .derive_child(ChildNumber(idx))
        .unwrap_throw();
    let private_key = child.private_key();
    // let public_key = child.public_key();
    // println!("private_key: {}", hex::encode(private_key.to_bytes()));
    // println!("public_key: {}", hex::encode(public_key.to_bytes()));
    private_key.to_bytes().try_into().unwrap_throw()
}

pub fn sign_digest(key: &[u8], digest: [u8; 32], idx_hd: Option<u32>) -> [u8; 65] {
    let secret_key = if let Some(idx) = idx_hd {
        SecretKey::parse(&key_from_entropy(&key, idx)).unwrap_throw()
    } else {
        SecretKey::parse(&key.try_into().unwrap_throw()).unwrap_throw()
    };
    let msg = Message::parse(&digest);
    let (signature, recovery_id) = sign(&msg, &secret_key);

    let mut sig_buf: [u8; 65] = [0; 65];
    sig_buf[0] = recovery_id.serialize();
    sig_buf[1..65].copy_from_slice(&signature.serialize());
    sig_buf
}

pub fn recover_public_key_from_signature(sig_buf: [u8; 65], digest: [u8; 32]) -> [u8; 33] {
    // Partisia Signature is 65 bytes
    let message = Message::parse(&digest);
    let signature =
        Signature::parse_standard(&sig_buf[1..65].try_into().unwrap_throw()).unwrap_throw();
    let recovery_id = RecoveryId::parse(sig_buf[0]).unwrap_throw();
    let public_key = recover(&message, &signature, &recovery_id).unwrap_throw();

    public_key.serialize_compressed()
}

pub fn public_key_to_address(public_key: [u8; 33]) -> [u8; 21] {
    // partisia address is the sha256 hash of the uncompressed public key
    let public_key_uncompressed: [u8; 65] = libsecp256k1::PublicKey::parse_slice(
        public_key.as_slice(),
        Some(PublicKeyFormat::Compressed),
    )
    .unwrap_throw()
    .serialize();

    let hash = digest::digest(&digest::SHA256, &public_key_uncompressed);
    let h: [u8; 32] = hash.as_ref().try_into().unwrap_throw();

    let mut address: [u8; 21] = [0; 21];
    address[1..21].copy_from_slice(&h[12..32]);
    address
}
