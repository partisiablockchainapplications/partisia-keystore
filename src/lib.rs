use std::convert::TryInto;

use keystore::{derive_key, KeyGen, KeyStore};
use serde::{Deserialize, Serialize};
use signer::sign_digest;
use wasm_bindgen::prelude::*;

mod keystore;
mod signer;
#[cfg(test)]
mod test;
mod utils;

// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
// allocator.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

#[wasm_bindgen]
pub fn generate_keystore(private_key: &[u8], password: &[u8], salt: &[u8], iv: &[u8]) -> KeyGen {
    keystore::keystore_generate(private_key, password, salt, iv)
}

#[derive(Deserialize, Serialize)]
struct JsResult {
    err: Option<String>,
    data: Option<Vec<u8>>,
}
#[wasm_bindgen]
pub fn is_keystore_password(keystore_json: &str, password: &[u8]) -> bool {
    if let Ok(keystore) = serde_json::from_str::<KeyStore>(keystore_json) {
        match derive_key(&keystore, password) {
            Ok(_) => true,
            _ => false,
        }
    } else {
        false
    }
}
#[wasm_bindgen(catch)]
pub fn decode_keystore(keystore_json: &str, password: &[u8]) -> Result<Vec<u8>, JsValue> {
    if let Ok(keystore) = serde_json::from_str::<KeyStore>(keystore_json) {
        match keystore::keystore_decode(&keystore, &password) {
            Ok(key) => Ok(key),
            Err(e) => Err(JsValue::from_str(&e)),
        }
    } else {
        Err(JsValue::from_str("malformed input"))
    }
}

#[wasm_bindgen(catch)]
pub fn sign(
    keystore_json: &str,
    password: &[u8],
    digest: &[u8],
    idx_hd: Option<u32>,
) -> Result<Vec<u8>, JsValue> {
    if digest.len() != 32 {
        return Err(JsValue::from_str("digest must be len of 32"));
    }
    if let Ok(keystore) = serde_json::from_str::<KeyStore>(keystore_json) {
        match keystore::keystore_decode(&keystore, &password) {
            Ok(key) => Ok(sign_digest(&key, digest.try_into().unwrap_throw(), idx_hd).to_vec()),
            Err(e) => Err(JsValue::from_str(&e)),
        }
    } else {
        Err(JsValue::from_str("malformed input"))
    }
}

#[wasm_bindgen(catch)]
pub fn sign_with_private_key(private_key: &[u8], digest: &[u8]) -> Result<Vec<u8>, JsValue> {
    if digest.len() != 32 {
        return Err(JsValue::from_str("digest must be len of 32"));
    }
    Ok(sign_digest(private_key, digest.try_into().unwrap_throw(), None).to_vec())
}

#[wasm_bindgen(catch)]
pub fn recover_public_key_from_signature(
    signature: &[u8],
    digest: &[u8],
) -> Result<Vec<u8>, JsValue> {
    if signature.len() != 65 {
        return Err(JsValue::from_str("signature must be len of 65"));
    }
    if digest.len() != 32 {
        return Err(JsValue::from_str("digest must be len of 32"));
    }

    Ok(signer::recover_public_key_from_signature(
        signature.try_into().unwrap(),
        digest.try_into().unwrap(),
    )
    .to_vec())
}
