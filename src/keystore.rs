const KDF_C: u32 = 131072; //1<<17

use aes_ctr::cipher::{
    generic_array::GenericArray,
    stream::{NewStreamCipher, SyncStreamCipher},
};
use aes_ctr::Aes256Ctr;
use core::num::NonZeroU32;
use hex::deserialize;
use ring::pbkdf2;
use serde::Deserialize;
use sha3::{Digest, Sha3_512};
use std::convert::TryInto;
use wasm_bindgen::{prelude::wasm_bindgen, UnwrapThrowExt};

#[wasm_bindgen]
#[derive(Deserialize)]
pub struct KeyStore {
    #[serde(deserialize_with = "deserialize")]
    cipher: Vec<u8>,
    #[serde(deserialize_with = "deserialize")]
    iv: Vec<u8>,
    #[serde(deserialize_with = "deserialize")]
    salt: Vec<u8>,
    #[serde(deserialize_with = "deserialize")]
    mac: Vec<u8>,
}

#[wasm_bindgen]
pub struct KeyGen {
    // length depends on how many words in the mnemonic
    cipher: Vec<u8>,
    mac: [u8; 64],
}

#[wasm_bindgen]
impl KeyGen {
    // fn new(cipher: &[u8], iv: &[u8], salt: &[u8], mac: &[u8]) -> Keystore {
    //     Keystore {
    //         cipher: cipher.to_vec(),
    //         iv: iv.to_vec(),
    //         salt: salt.to_vec(),
    //         mac: mac.to_vec(),
    //     }
    // }
    #[wasm_bindgen(getter)]
    pub fn get_cipher(&self) -> Vec<u8> {
        self.cipher.to_vec()
    }
    #[wasm_bindgen(getter)]
    pub fn get_mac(&self) -> Vec<u8> {
        self.mac.to_vec()
    }
}

pub fn derive_key(keystore: &KeyStore, password: &[u8]) -> Result<[u8; 32], String> {
    if keystore.mac.len() != 64 {
        return Err("invalid mac length".to_string());
    }
    let mut derived_key: [u8; 32] = [0_u8; 32];
    {
        let algorithm = pbkdf2::PBKDF2_HMAC_SHA256;
        let iterations = NonZeroU32::new(KDF_C).unwrap_throw();
        pbkdf2::derive(
            algorithm,
            iterations,
            &keystore.salt,
            &password,
            &mut derived_key,
        );
    }
    let buf_value: Vec<u8> = [derived_key[16..32].to_vec(), keystore.cipher[..].to_vec()].concat();
    let mut sha3_hasher = Sha3_512::new();
    sha3_hasher.update(&buf_value);

    let hex_mac1: &[u8] = &sha3_hasher.finalize().to_vec();
    let hex_mac2: &[u8] = &keystore.mac.to_vec();

    if !hex_mac1.iter().zip(hex_mac2.iter()).all(|(a, b)| a == b) {
        return Err("password is incorrect".to_string());
    }
    Ok(derived_key)
}

pub fn keystore_decode(keystore: &KeyStore, password: &[u8]) -> Result<Vec<u8>, String> {
    let derived_key = derive_key(keystore, password)?;
    // password is valid so now decrypt
    let mut cipher = Aes256Ctr::new(
        GenericArray::from_slice(&derived_key),
        GenericArray::from_slice(&keystore.iv),
    );
    let mut data = keystore.cipher.to_vec();
    cipher.apply_keystream(&mut data);
    Ok(data)
}
pub fn keystore_generate(private_key: &[u8], password: &[u8], salt: &[u8], iv: &[u8]) -> KeyGen {
    let algorithm = pbkdf2::PBKDF2_HMAC_SHA256;
    let iterations = NonZeroU32::new(KDF_C).unwrap_throw();

    let mut derived_key: [u8; 32] = [0; 32];
    {
        pbkdf2::derive(algorithm, iterations, &salt, &password, &mut derived_key);
    }
    let key = GenericArray::from_slice(&derived_key);
    let nonce = GenericArray::from_slice(&iv);
    let mut aes = Aes256Ctr::new(&key, &nonce);

    let mut data = private_key.to_vec();
    aes.apply_keystream(&mut data);
    let buf_value = [&derived_key[16..32], data.as_slice()].concat();

    let mut sha3_hasher = Sha3_512::new();
    sha3_hasher.update(&buf_value);

    let key_gen = KeyGen {
        cipher: data,
        mac: sha3_hasher.finalize().as_slice().try_into().unwrap_throw(),
    };
    key_gen
}

// #[cfg(test)]
// mod tests {
//     use super::*;
//     // random salt and iv
//     const SALT: [u8; 32] = [
//         10, 248, 213, 37, 163, 90, 27, 122, 157, 20, 201, 174, 161, 227, 158, 70, 29, 84, 9, 205,
//         26, 219, 111, 162, 117, 183, 40, 197, 234, 235, 106, 162,
//     ];
//     const IV: [u8; 16] = [
//         191, 229, 18, 143, 24, 47, 214, 160, 201, 90, 236, 32, 85, 67, 218, 171,
//     ];
//     #[test]
//     fn test_gen_keystore() {
//         let password = b"Welcome1!";
//         let private_key = vec![1, 2, 3, 4, 5];
//         let key_store = rust_keystore_generate(&private_key, password, &SALT, &IV);
//         let hex_ciper = hex::encode(&key_store.cipher);
//         let hex_max = hex::encode(&key_store.mac);
//         assert_eq!(hex_ciper, "605caeeeca");
//         assert_eq!(hex_max, "e688f8c304fd70a406d07bba80bcede1d082148465e3720c1dfb592fb37d5c54b0ec97912048a9cad346d1cfa4de31b0c2468edbd054f5558c2ed15f01d9684f");
//     }
//     #[test]
//     fn test_decrypt_keystore() {
//         let password = b"Welcome1!";
//         let keystore = Keystore::new(
//             &hex::decode("605caeeeca").unwrap(),
//             &IV.to_vec(),
//             &SALT.to_vec(),
//             &hex::decode("e688f8c304fd70a406d07bba80bcede1d082148465e3720c1dfb592fb37d5c54b0ec97912048a9cad346d1cfa4de31b0c2468edbd054f5558c2ed15f01d9684f").unwrap(),
//         );
//         let res_success = rust_keystore_decrypt(&keystore, password);
//         assert!(res_success.is_ok());
//         assert_eq!(hex::encode(res_success.unwrap()), "0102030405");

//         // do it with the wrong password
//         let res_fail = rust_keystore_decrypt(&keystore, b"not the password");
//         assert!(res_fail.is_err());
//         assert_eq!(res_fail.err().unwrap(), "password is incorrect");
//     }
// }
