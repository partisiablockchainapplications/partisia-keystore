import { generate_keystore, sign, decode_keystore, recover_public_key_from_signature } from 'partisia-rust-keystore';
import crypto from 'crypto';
import assert from 'assert';
import jssha3 from 'js-sha3';

// (() => {
//     // const password = Buffer.from("Welcome1")
//     // const salt = Buffer.from([
//     //     17, 59, 186, 132, 136, 168, 135, 0, 6, 125, 58, 43, 231, 209, 178, 88, 123, 111, 1, 250,
//     //     65, 39, 252, 49, 94, 242, 14, 79, 5, 7, 29, 216,
//     // ])// crypto.randomBytes(32);
//     // const pw_hash = scryptsy_encrypt(password, salt);
//     // console.log('pw_hash', pw_hash)

//     // derive the key from wasm
//     const password = Buffer.from("Welcome1")
//     const priv_key = Buffer.from('f51c7fb358a1898c72a13fe67630e54e8b508242b49358f5020782a34b878b74', 'hex')//crypto.randomBytes(32)
//     const rng_salt = Buffer.from([
//         17, 59, 186, 132, 136, 168, 135, 0, 6, 125, 58, 43, 231, 209, 178, 88, 123, 111, 1, 250,
//         65, 39, 252, 49, 94, 242, 14, 79, 5, 7, 29, 216,
//     ])// crypto.randomBytes(32);
//     const rng_iv = Buffer.from([186, 47, 213, 240, 233, 46, 8, 76, 167, 180, 141, 35, 7, 178, 221, 120])//crypto.randomBytes(16);
//     const now1 = Date.now()
//     const bufRustKey = generate_keystore(priv_key, password, rng_salt, rng_iv)
//     console.log('time wasm', Date.now() - now1)
//     console.log('bufRustKey', JSON.stringify(bufRustKey, null, 2))
//     console.log(Buffer.from(bufRustKey.get_cipher).toString('hex'))
//     console.log(Buffer.from(bufRustKey.get_mac).toString('hex'))
//     console.log(bufRustKey.free())
//     // console.log(bufRustKey.get_iv)
//     // const { cipher, iv, salt, mac } = bufRustKey
// })();



(() => {
    const password = Buffer.from('Welcome1')
    const keystore = {
        iv: "ba2fd5f0e92e084ca7b48d2307b2dd78",
        salt: "113bba8488a88700067d3a2be7d1b2587b6f01fa4127fc315ef20e4f05071dd8",
        cipher: "c83148780a7d4ce0fa5eb92f32aee86ff4b7ee592c359336304b7742a9c84064",
        mac: "a878fff5252a254645b9c3f0d09ded1b335d05e778979b0a60ea44c5051f7e8314b232e90023c31eefc14dc7dbca03f0837833dbaec1f6fb57b1ef9853403556"
    }

    const wif = decode_keystore(JSON.stringify(keystore), password)
    console.log('wif', Buffer.from(wif).toString('hex'))

    // wasm: 014d649d7484f75d196dee278430694fcc09a59849f37f2a1ec8f50223ae2bda7c6c7d850351b1b4df29ed9ae51097b9c151b823939d66b9d2caba91810a785ab6
    // wasm is canonical; adjusted nodejs also
    // node: 004d649d7484f75d196dee278430694fcc09a59849f37f2a1ec8f50223ae2bda7c93827afcae4e4b20d612651aef68463d68f6b95311e1e668f517cd0bc5bde68b

    const digest = Buffer.from("0c8208ea4e3a95be34c063a473a8d285f84371b1f4d3f996bcf07bb5f65f3781", "hex")
    try {
        const sig = sign(JSON.stringify(keystore), password, digest)
        console.log('sig', Buffer.from(sig).toString('hex'))

        const pub_key = recover_public_key_from_signature(Buffer.from(sig), digest)
        console.log('pub_key', Buffer.from(pub_key).toString('hex'))
    } catch (msg) {
        console.error(msg)
    }
    // assert(priv_key.toString('hex') == Buffer.from(privKey).toString('hex'))
})()

